# Capistrano::Rust

rustup/cargo/rustc support for Capistrano 3.x

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'capistrano', '~> 3.1.0'
gem 'capistrano-rust'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install capistrano-rust

## Usage

Require in `Capfile` to use the default task:

```ruby
require 'capistrano/rust'
```

Configurable options:

```ruby
set :rustup_custom_env, '$HOME/.cargo/env'
set :rustup_roles, :all
set :rustup_map_bins, %w{rustc cargo}
```

## Tasks

### rustup:exec

Run command in the rustup environment.

You can override any of these defaults by setting the variables shown below.

```ruby
set :cargo_target_path, nil
set :rustup_exec_roles, :all
set :rustup_exec_env_variables, {}
```

This task is strictly opt-in. If you want to run it on every deployment
before you run cargo build, add the following to your deploy.rb.

```ruby
set :rustup_map_bins, %w{rustc cargo diesel}

after 'cargo:install', :run_migration do
  set :rustup_exec_cmd, 'diesel migration run'
  invoke 'rustup:exec'
end
```

### cargo:install

Install cargo packages.

You can override any of these defaults by setting the variables shown below.

```ruby
set :cargo_target_path, nil
set :cargo_install_roles, :all
set :cargo_install_env_variables, {}
set :cargo_install_crates, []
```

`cargo_install_crates` is and Array of String or Hash :

```ruby
set :cargo_install_crates, ['diesel_cli', {name: 'cargo-watch', flags: ['-f']}]
```

This task is strictly opt-in. If you want to run it on every deployment
before you run cargo build, add the following to your deploy.rb.

```ruby
before 'cargo:build', 'cargo:install'
```

### cargo:build


Install the project dependencies via cargo, and build the project.

You can override any of these defaults by setting the variables shown below.

```ruby
set :cargo_target_path, nil
set :cargo_build_roles, :all
set :cargo_build_env_variables, {}
set :cargo_build_flags, ['--release']
```

## Development

After checking out the repo, run `bundle install` to install dependencies.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/glejeune/capistrano-rust. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Code of Conduct

Everyone interacting in the Capistrano::Rust project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/glejeune/capistrano-rust/blob/master/CODE_OF_CONDUCT.md).
