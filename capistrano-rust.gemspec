# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano/rust/version'

Gem::Specification.new do |spec|
  spec.name          = 'capistrano-rust'
  spec.version       = Capistrano::Rust::VERSION
  spec.authors       = ['Grégoire Lejeune']
  spec.email         = ['gregoire.lejeune@gmail.com']

  spec.summary       = 'Rustup/Rustc/Cargo support for Capistrano 3.x'
  spec.description   = 'Rustup/Rustc/Cargo support for Capistrano 3.x'
  spec.homepage      = 'https://gitlab.com/glejeune/capistrano-rust'

  spec.license       = 'BSD-3-Clause'

  spec.files         = `git ls-files`.split($/)
  spec.require_paths = ['lib']

  spec.add_dependency 'capistrano', '~> 3.1'

  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'rake', '~> 10.0'
end
