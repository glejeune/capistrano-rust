# frozen_string_literal: true

module Capistrano
  module Rust
    VERSION = '0.1.0'
  end
end
